#ifndef __BME280_H
#define __BME280_H

#define DRIVER_NAME "bme280"

/* Defined for device identification */
#define I2C_BUS_AVAILABLE 1
#define SLAVE_DEVICE_NAME "BME280"
#define BME280_SLAVE_ADDRESS 0x76
#define DEVICE_ID 0x60

/* special value */
#define BME280_SOFT_RESET 0xB6

/* Address of registers */

/* Memory map */
#define HUM_LSB     0xFE
#define HUM_MSB     0xFD

#define TEMP_XLSB   0xFC
#define TEMP_LSB    0xFB
#define TEMP_MSB    0xFA

#define PRESS_XLSB  0xF9
#define PRESS_LSB   0xF8
#define PRESS_MSB   0xF7

#define CONFIG      0xF5

#define CTRL_MEAS   0xF4

#define STATUS      0xF3

#define CTRL_HUM    0xF2

#define RESET       0xE0
#define ID          0xD0

/* Temperature calibration values registers address*/
#define DIG_T1_W    0x88 // 0x88/0x89
#define DIG_T2_W    0x8A // 0x8A/0x8B
#define DIG_T3_W    0x8C // 0x8C/0x8D

/* Pressure calibration values registers address */
#define DIG_P1_W    0x8E // 0x8E/0x8F
#define DIG_P2_W    0x90 // 0x90/0x91
#define DIG_P3_W    0x92 // 0x92/0x93
#define DIG_P4_W    0x94 // 0x94/0x95
#define DIG_P5_W    0x96 // 0x96/0x97
#define DIG_P6_W    0x98 // 0x98/0x99
#define DIG_P7_W    0x9A // 0x9A/0x9B
#define DIG_P8_W    0x9C // 0x9C/0x9D
#define DIG_P9_W    0x9E // 0x9E/0x9F

/* Humidity calibration values registers address */
#define DIG_H1      0xA1
#define DIG_H2_W    0xE1 // 0xE1/0xE2
#define DIG_H3      0xE3
#define DIG_H4_W    0xE4 // 0xE4/0xE5
#define DIG_H5_W    0xE6 // 0xE6/0xE7
#define DIG_H6      0xE7

#endif // __BME280_H
