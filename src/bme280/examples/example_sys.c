
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#define SYS_PATH "/sys/class/bme280/"


int update_sys_conf(char *filename, u_int8_t reg)
{
        FILE *sys;
        char buf[8] = "";
        sys = fopen(filename, "w");
        if (sys == NULL){
                perror ("Error open");
                return -1;
        }
        snprintf(buf, 7, "0x%02X\n",reg);
        fputs(buf, sys);
        fclose(sys);
}

long get_value(char *buf){
        char *end, *temp_s;
        long temp;
        temp_s = strtok(buf,":\n");
        temp_s = strtok(NULL,":\n");
        return strtol(temp_s, &end, 10);
}

int main (void)
{
        char *end, *temp_s, buf[80] = "";
        long temp;
        double temperature, pressure;
        FILE *fd;
        update_sys_conf(SYS_PATH "config",((4 << 5) | (4 << 2)));
        update_sys_conf(SYS_PATH "ctrl_meas",((4 << 5) | (4 << 2) | 3));
        update_sys_conf(SYS_PATH "ctrl_hum", 4);
	while(1){
                fd = fopen ("/dev/bme280", "r");
                if (fd == NULL) {
                        perror ("Error open");
                        return -1;
                }
		fgets(buf, 50, fd);
		printf("%s", buf);

                fgets(buf, 50, fd);
                temperature = (double)(get_value(buf))/100;
                printf("Temperature: %.2f C\n", temperature);

                fgets(buf, 50, fd);
                pressure = (double)(get_value(buf))/256;
                printf("Pressure: %.2f mom\n", pressure/133);
                printf("Altitude: %.2f m\n", (double)((pow(101325/pressure, 1/5.257) - 1)*(temperature + 273.15)/0.0065));

                fgets(buf, 50, fd);
                printf("Humidity: %.2f\%\n\n", (float)(get_value(buf))/1042);


                fclose(fd);
                sleep(5);
        }
        return 0;
}

