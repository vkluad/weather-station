# BME280 char device driver

## How to use
1) Compile driver in target machine, or use directory with linux-headers, and some parameters
for this PC.
- Run `make all` in target machine.
- Run `make KDIR=${BUILD_DIR} all` for cross-compile.
2) Run this module
- `sudo insmod bme280.ko`
3) Module will create few files in `/sys/class/` and `/dev/` directory,
in `/sys/class/` we can see `bme280` dir, and this dir containe 4
files `config`, `ctrl_meas`, `ctrl_hum`, and folder `bme280` who containe char device info.
- `config`, `ctrl_meas`, and `ctrl_hum` responsible for similar registers, will be careful to use it.
- file `bme280` in `/dev` is char device file, you can read and can't write in this file.

Command `sudo cat /dev/bme280` will show the following:
```
Time: 10.10.2021 15:47:10
Temperature: 2170
Pressure: 25271935
Humidity: 83238
```
For calculate this value to normal vision use next formulas:
- Temperature : 2170/10 = 21.70 C
- Pressure : 25271935/256 = 98718.5 Pa = 98718.5/133.3 = 740.5 mom
- Humidity : 83238 / 1024 = 81.28%

## For setting up registers
Write only hex numbers in format `0xFF` to registers files\
For example:
```
echo "0xAC" > /sys/class/bme280/config
```

## Use in code
Use examples for know it.
