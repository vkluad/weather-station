#include "bme280.h"
#include <linux/math64.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/types.h>
#include <linux/i2c.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/sysfs.h>
#include <linux/kernel.h>
#include <linux/uaccess.h>
#include <linux/cdev.h>
#include <linux/delay.h>
#include <linux/timekeeping.h>

static int major;

static int config_param = (5 << 5) | (3 << 2),
	   ctrl_meas_param = (5 << 5) | (5 << 2) | 3, ctrl_hum_param = 5;

module_param(config_param, int, S_IRUGO);
module_param(ctrl_meas_param, int, S_IRUGO);
module_param(ctrl_hum_param, int, S_IRUGO);

struct bme280_calib {
	u16 dig_T1;
	s16 dig_T2;
	s16 dig_T3;

	u16 dig_P1;
	s16 dig_P2;
	s16 dig_P3;
	s16 dig_P4;
	s16 dig_P5;
	s16 dig_P6;
	s16 dig_P7;
	s16 dig_P8;
	s16 dig_P9;

	u8  dig_H1;
	s16 dig_H2;
	u8  dig_H3;
	s16 dig_H4;
	s16 dig_H5;
	s8  dig_H6;
};

struct bme280_data {
	struct i2c_client *bme280_i2c_client;
	struct bme280_calib *calib;

	u8 config; 	// config register
	u8 ctrl_meas; 	// ctrl_meas register
	u8 ctrl_hum; 	// ctrl_hum register

	int (*chip_config)(struct bme280_data *);
	s32 (*read_temp)(struct bme280_data *);
	u32 (*read_press)(struct bme280_data *);
	u32 (*read_hum)(struct bme280_data *);
	void (*set_calib_data)(struct bme280_data *);
	s32 t_fine;
};

static int chip_config(struct bme280_data *data)
{
	int ret;

	ret = i2c_smbus_write_byte_data(data->bme280_i2c_client,
					RESET,
					BME280_SOFT_RESET); /* Soft reset */
	if (ret < 0)
		goto err;
	msleep(4); /* Safe sleep for hard reset */

	ret = i2c_smbus_write_byte_data(data->bme280_i2c_client,
					CONFIG,
					data->config); /* write config register */
	if (ret < 0)
		goto err;

	ret = i2c_smbus_write_byte_data(data->bme280_i2c_client,
					CTRL_MEAS,
					data->ctrl_meas); /* write ctrl_meas register */
	if (ret < 0)
		goto err;

	ret = i2c_smbus_write_byte_data(
		data->bme280_i2c_client, CTRL_HUM,
		data->ctrl_hum); /* write ctrl_hum register */
err:
	return ret;
}

static void set_calib_data(struct bme280_data *data)
{
	struct bme280_calib *calib = data->calib;

	/* Read compensate temperature value*/
	calib->dig_T1 =	i2c_smbus_read_word_data(data->bme280_i2c_client, DIG_T1_W);
	calib->dig_T2 =	i2c_smbus_read_word_data(data->bme280_i2c_client, DIG_T2_W);
	calib->dig_T3 =	i2c_smbus_read_word_data(data->bme280_i2c_client, DIG_T3_W);

	/* Read compensate humidity value */
	calib->dig_H1 =	i2c_smbus_read_byte_data(data->bme280_i2c_client, DIG_H1);
	calib->dig_H2 =	i2c_smbus_read_word_data(data->bme280_i2c_client, DIG_H2_W);
	calib->dig_H3 =	i2c_smbus_read_byte_data(data->bme280_i2c_client, DIG_H3);
	calib->dig_H4 =	i2c_smbus_read_word_data(data->bme280_i2c_client, DIG_H4_W);
	calib->dig_H5 = i2c_smbus_read_word_data(data->bme280_i2c_client, DIG_H5_W);
	calib->dig_H6 =	i2c_smbus_read_byte_data(data->bme280_i2c_client, DIG_H6);

	/* Read conpensate pressure value */
	calib->dig_P1 =	i2c_smbus_read_word_data(data->bme280_i2c_client, DIG_P1_W);
	calib->dig_P2 =	i2c_smbus_read_word_data(data->bme280_i2c_client, DIG_P2_W);
	calib->dig_P3 =	i2c_smbus_read_word_data(data->bme280_i2c_client, DIG_P3_W);
	calib->dig_P4 =	i2c_smbus_read_word_data(data->bme280_i2c_client, DIG_P4_W);
	calib->dig_P5 =	i2c_smbus_read_word_data(data->bme280_i2c_client, DIG_P5_W);
	calib->dig_P6 =	i2c_smbus_read_word_data(data->bme280_i2c_client, DIG_P6_W);
	calib->dig_P7 =	i2c_smbus_read_word_data(data->bme280_i2c_client, DIG_P7_W);
	calib->dig_P8 =	i2c_smbus_read_word_data(data->bme280_i2c_client, DIG_P8_W);
	calib->dig_P9 =	i2c_smbus_read_word_data(data->bme280_i2c_client, DIG_P9_W);
}

// Returns temperature in DegC, resolution is 0.01 DegC. Output value of “5123” equals 51.23 DegC.
// t_fine carries fine temperature as global value
s32 read_temperature_raw(struct bme280_data *data)
{
	s32 raw_T, temp_msb, temp_lsb, temp_xlsb;

	if ((data->ctrl_meas & 0xE0) == 0)
		return 0;
	/* Read Temperature */
	temp_msb = i2c_smbus_read_byte_data(data->bme280_i2c_client, TEMP_MSB);

	if ((temp_msb == 0x80) && ((data->ctrl_meas & 0xE0) != 0x00)) {
		data->chip_config(data);
		return 0;
	}
	if ((temp_msb == 0x80) && ((data->ctrl_meas & 0xE0) == 0))
		return 0;

	temp_lsb = i2c_smbus_read_byte_data(data->bme280_i2c_client, TEMP_LSB);
	temp_xlsb =
		i2c_smbus_read_byte_data(data->bme280_i2c_client, TEMP_XLSB);
	raw_T = (((s32)temp_msb << 16) | ((s32)temp_lsb << 8) | temp_xlsb) >> 4;

	return raw_T;
}

s32 read_pressure_raw(struct bme280_data *data)
{
	s32 raw_P, press_msb, press_lsb, press_xlsb;

	if ((data->ctrl_meas & 0x1C) == 0)
		return 0;

	/* Read pressure */
	press_msb =
		i2c_smbus_read_byte_data(data->bme280_i2c_client, PRESS_MSB);

	if ((press_msb == 0x80) && ((data->ctrl_meas & 0x1C) != 0)) {
		data->chip_config(data);
		return 0;
	}

	if ((press_msb == 0x80) && ((data->ctrl_meas & 0x1C) == 0))
		return 0;

	press_lsb =
		i2c_smbus_read_byte_data(data->bme280_i2c_client, PRESS_LSB);
	press_xlsb =
		i2c_smbus_read_byte_data(data->bme280_i2c_client, PRESS_XLSB);
	raw_P = (((s32)press_msb << 16) | ((s32)press_lsb << 8) | (s32)press_xlsb) >> 4;

	return raw_P;
}

s32 read_humidity_raw(struct bme280_data *data)
{
	s32 raw_H, hum_msb, hum_lsb;

	if ((data->ctrl_meas & 0x03) == 0)
		return 0;

	if ((data->ctrl_hum & 0x07) == 0)
		return 0;

	/* Read humidity */
	hum_msb = i2c_smbus_read_byte_data(data->bme280_i2c_client, HUM_MSB);

	hum_lsb = i2c_smbus_read_byte_data(data->bme280_i2c_client, HUM_LSB);
	raw_H = (hum_msb << 8) | hum_lsb;

	return raw_H;
}

// Returns temperature in DegC, resolution is 0.01 DegC. Output value of “5123” equals 51.23DegC.
// t_fine carries fine temperature as global value
s32 bme280_compensate_temperature(struct bme280_data *data)
{
	s32 var1, var2, T, adc_T = read_temperature_raw(data);
	struct bme280_calib *calib = data->calib;

	var1 = (((adc_T >> 3) - ((s32)le16_to_cpu(calib->dig_T1) << 1)) *
		((s32)(s16)le16_to_cpu(calib->dig_T2))) >> 11;

	var2 = (((((adc_T >> 4) - ((s32)le16_to_cpu(calib->dig_T1))) *
		  ((adc_T >> 4) - ((s32)le16_to_cpu(calib->dig_T1)))) >> 12) *
		((s32)(s16)le16_to_cpu(calib->dig_T3))) >> 14;

	data->t_fine = var1 + var2;
	T = (data->t_fine * 5 + 128) >> 8;
	return T;
}

// Returns pressure in Pa as unsigned 32 bit integer in Q24.8 format (24 integer bits and 8 fractional bits).
// Output value of “24674867” represents 24674867/256 = 96386.2 Pa = 963.862 hPa

u32 bme280_compensate_pressure(struct bme280_data *data)
{
	s64 var1, var2, p;
	s32 adc_P = read_pressure_raw(data);
	struct bme280_calib *calib = data->calib;

	var1 = ((s64)data->t_fine) - 128000;
	var2 = var1 * var1 * (s64)(s16)le16_to_cpu(calib->dig_P6);
	var2 += (var1 * (s64)(s16)le16_to_cpu(calib->dig_P5)) << 17;
	var2 += ((s64)(s16)le16_to_cpu(calib->dig_P4)) << 35;
	var1 = ((var1 * var1 * (s64)(s16)le16_to_cpu(calib->dig_P3)) >> 8) +
	       ((var1 * (s64)(s16)le16_to_cpu(calib->dig_P2)) << 12);
	var1 = ((((s64)1) << 47) + var1) * ((s64)le16_to_cpu(calib->dig_P1)) >> 33;

	if (var1 == 0)
		return 0;

	p = ((((s64)1048576 - adc_P) << 31) - var2) * 3125;
	p = div64_s64(p, var1);
	var1 = (((s64)(s16)le16_to_cpu(calib->dig_P9)) * (p >> 13) *
		(p >> 13)) >> 25;

	var2 = (((s64)(s16)le16_to_cpu(calib->dig_P8)) * p) >> 19;
	p = ((p + var1 + var2) >> 8) + (((s64)(s16)le16_to_cpu(calib->dig_P7)) << 4);

	return (u32)p;
}
// Returns humidity in %RH as unsigned 32 bit integer in Q22.10 format (22 integer and 10 fractional bits).
// Output value of “47445” represents 47445/1024 = 46.333 %RH

u32 bme280_compensate_humidity(struct bme280_data *data)
{
	unsigned int H1, H3, tmp;
	int H2, H4, H5, H6, var, adc_H = read_humidity_raw(data);
	struct bme280_calib *calib = data->calib;

	H1 = calib->dig_H1;

	tmp = calib->dig_H2;
	H2 = sign_extend32(le16_to_cpu(tmp), 15);

	H3 = calib->dig_H3;

	tmp = calib->dig_H4;
	H4 = sign_extend32(((be16_to_cpu(tmp) >> 4) & 0xff0) |
				   (be16_to_cpu(tmp) & 0xf), 11);

	tmp = calib->dig_H5;
	H5 = sign_extend32(((le16_to_cpu(tmp) >> 4) & 0xfff), 11);

	tmp = calib->dig_H6;
	H6 = sign_extend32(tmp, 7);

	var = ((s32)data->t_fine) - (s32)76800;
	var = ((((adc_H << 14) - (H4 << 20) - (H5 * var)) + (s32)16384) >> 15) *
	      (((((((var * H6) >> 10) * (((var * (s32)H3) >> 11) +
	      (s32)32768)) >> 10) + (s32)2097152) * H2 + 8192) >> 14);
	var -= ((((var >> 15) * (var >> 15)) >> 7) * (s32)H1) >> 4;
	var = var >> 12;
	var = (var < 0 ? 0 : var);
	var = (var > 102400 ? 102400 : var);
	return var;
}

int update_config(struct bme280_data *data, const char *buf, u8 up_v)
{
	unsigned long new_mode = 0;

	if (strstr(buf, "0x") == NULL)
		return -1;

	if (kstrtol(buf, 16, &new_mode)) {
		printk(KERN_ERR DRIVER_NAME
		       ": only numbers enter, but you enter: %s",
		       buf);
		return -1;
	}
	if (new_mode > 0xFF)
		return -1;

	if (up_v == CONFIG)
		data->config = new_mode;

	if (up_v == CTRL_MEAS)
		data->ctrl_meas = new_mode;

	if (up_v == CTRL_HUM)
		data->ctrl_hum = new_mode;

	i2c_smbus_write_byte_data(data->bme280_i2c_client, up_v, (u8)new_mode);
	return 0;
}

static struct bme280_data data;

static int bme280_i2c_probe(struct i2c_client *client)
{

	data.bme280_i2c_client = client;
	data.chip_config = chip_config;
	data.read_temp = bme280_compensate_temperature;
	data.read_press = bme280_compensate_pressure;
	data.read_hum = bme280_compensate_humidity;

	data.calib = kmalloc(sizeof(struct bme280_calib), GFP_KERNEL);

	data.config = config_param;
	data.ctrl_meas = ctrl_meas_param;
	data.ctrl_hum = ctrl_hum_param;
	data.set_calib_data = set_calib_data;

	data.chip_config(&data);
	data.set_calib_data(&data);

	return 0;
}

static int bme280_i2c_remove(struct i2c_client *client)
{
	if(data.calib != NULL)
		kfree(data.calib);
	return 0;
}

ssize_t config_store(struct class *class, struct class_attribute *attr,
		     const char *buf, size_t count)
{
	int ret;

	ret = update_config(&data, buf, CONFIG);
	if (ret < 0)
		return ret;

	return count;
}

ssize_t config_show(struct class *class, struct class_attribute *attr,
		    char *buf)
{
	char out_str[8] = "";
	snprintf(out_str, 7, "0x%02X\n", data.config);

	if (!strncpy(buf, out_str, strlen(out_str)))
		return -EINVAL;

	return strlen(out_str);
}

ssize_t ctrl_meas_store(struct class *class, struct class_attribute *attr,
			const char *buf, size_t count)
{
	int ret;
	ret = update_config(&data, buf, CTRL_MEAS);

	if (ret < 0)
		return ret;

	return count;
}

ssize_t ctrl_meas_show(struct class *class, struct class_attribute *attr,
		       char *buf)
{
	char out_str[8] = "";
	snprintf(out_str, 7, "0x%02X\n", data.ctrl_meas);

	if (!strncpy(buf, out_str, strlen(out_str)))
		return -EINVAL;

	return strlen(out_str);
}

ssize_t ctrl_hum_store(struct class *class, struct class_attribute *attr,
		       const char *buf, size_t count)
{
	int ret;
	ret = update_config(&data, buf, CTRL_HUM);

	if (ret < 0)
		return ret;

	return count;
}

ssize_t ctrl_hum_show(struct class *class, struct class_attribute *attr,
		      char *buf)
{
	char out_str[8] = "";
	snprintf(out_str, 7, "0x%02X\n", data.ctrl_hum);

	if (!strncpy(buf, out_str, strlen(out_str)))
		return -EINVAL;

	return strlen(out_str);
}

CLASS_ATTR_RW(config);
CLASS_ATTR_RW(ctrl_meas);
CLASS_ATTR_RW(ctrl_hum);

static ssize_t bme280_read(struct file *file, char __user *buf, size_t lenght,
			   loff_t *pos)
{
	int len;
	char out_string[128];
	s32 temperature;
	u32 pressure, humidity;
	struct tm tm_now;
	time64_to_tm(ktime_get_real_seconds()+3*3600, 0, &tm_now);
	temperature = data.read_temp(&data);
	pressure = data.read_press(&data);
	humidity = data.read_hum(&data);

	snprintf(out_string, sizeof(out_string),
		 "Time: %02d.%02d.%04ld %02d:%02d:%02d\nTemperature: %d\nPressure: %d\nHumidity: %d\n", 
		 tm_now.tm_mday, tm_now.tm_mon+1, tm_now.tm_year+1900,
	         tm_now.tm_hour, tm_now.tm_min, tm_now.tm_sec,
		 temperature,
		 pressure, humidity);
	len = strlen(out_string);
	if (lenght < len)
		return -EINVAL;

	if (*pos != 0) {
		*pos = 0;
		return 0;
	}

	if (copy_to_user(buf, out_string, len))
		return -EINVAL;

	*pos = len;

	return len;
}

static const struct file_operations bme280_cdev_fops = {
	.owner = THIS_MODULE,
	.read = bme280_read,
};

static const struct of_device_id bme280_of_i2c_match[] = {
	{ .compatible = "bosch,bme280", .data = (void *)DEVICE_ID },
	{},
};
MODULE_DEVICE_TABLE(of, bme280_of_i2c_match);

static struct i2c_device_id bme280_i2c_id[] = {
	{ DRIVER_NAME, DEVICE_ID },
	{},
};
MODULE_DEVICE_TABLE(i2c, bme280_i2c_id);

static struct i2c_driver bme280_i2c_driver = {
	.driver = {
		.name = DRIVER_NAME,
                .of_match_table = of_match_ptr(bme280_of_i2c_match),

	},
	.probe_new = bme280_i2c_probe,
	.remove    = bme280_i2c_remove,
	.id_table  = bme280_i2c_id,
};

static struct class *bme280_class;
static struct cdev bme280_cdev;

static int __init bme280_driver_init(void)
{
	int ret;
	dev_t dev;

	ret = i2c_add_driver(&bme280_i2c_driver);
	if (ret) {
		printk(DRIVER_NAME ": failed to add new i2c driver");
		return ret;
	}

	ret = alloc_chrdev_region(&dev, 0, 1, DRIVER_NAME);

	if (ret < 0) {
		printk(KERN_ERR DRIVER_NAME
		       ": Device could not be allocated!\n");
		return ret;
	}
	major = MAJOR(dev);

	cdev_init(&bme280_cdev, &bme280_cdev_fops);
	bme280_cdev.owner = THIS_MODULE;
	ret = cdev_add(&bme280_cdev, dev, 1);

	if (ret < 0) {
		printk(KERN_ERR DRIVER_NAME ": Can not add char device\n");
		goto dev_err;
	}

	bme280_class = class_create(THIS_MODULE, DRIVER_NAME);

	if (IS_ERR(bme280_class)) {
		printk(KERN_ERR DRIVER_NAME ": bad class sysfs create\n");
		goto cdev_err;
	}

	device_create(bme280_class, NULL, dev, NULL,
		      DRIVER_NAME); // Create cdev

	if ((ret = class_create_file(
		     bme280_class,
		     &class_attr_config))) // create sysfs config file
		goto class_err;

	if ((ret = class_create_file(
		     bme280_class,
		     &class_attr_ctrl_meas))) // create sysfs ctrl_meas file
		goto config_err;

	if ((ret = class_create_file(
		     bme280_class,
		     &class_attr_ctrl_hum))) // create sysfs ctrl_hum file
		goto ctrl_meas_err;

	return 0;

ctrl_meas_err:
	class_remove_file(bme280_class, &class_attr_ctrl_meas);

config_err:
	class_remove_file(bme280_class, &class_attr_config);

class_err:
	device_destroy(bme280_class, dev);
	class_destroy(bme280_class);

cdev_err:
	cdev_del(&bme280_cdev);

dev_err:
	unregister_chrdev_region(MKDEV(major, 0), 1);

	return ret;
}
module_init(bme280_driver_init);

static void __exit bme280_driver_exit(void)
{
	dev_t dev;
	dev = MKDEV(major, 0);

	device_destroy(bme280_class, dev);
	class_remove_file(bme280_class, &class_attr_config);
	class_remove_file(bme280_class, &class_attr_ctrl_meas);
	class_remove_file(bme280_class, &class_attr_ctrl_hum);

	class_destroy(bme280_class);
	cdev_del(&bme280_cdev);
	unregister_chrdev_region(MKDEV(major, 0), 1);
	i2c_del_driver(&bme280_i2c_driver);
}
module_exit(bme280_driver_exit);

MODULE_AUTHOR("Vlad Kudenchuk <vkluad@gmail.com>");
MODULE_DESCRIPTION("BME280 /dev entries driver");
MODULE_LICENSE("GPL");
