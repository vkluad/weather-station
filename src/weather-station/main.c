#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

void update_screen(double temp, int press, int alt, double hum, char *time)
{
	FILE *ili9341;
	char buf[1024] = "";
	int r0, b0;
	double delta;
	delta = 1.0/35.0*temp;

	if(delta >= 1)
		delta = 1;
	else if(delta < 0)
		delta = 0;

	r0 = 255 * delta;
	b0 = 255 - 255 * delta;

	ili9341 = fopen ("/dev/ili9341", "w");
	if (ili9341 == NULL) {
		perror ("Error open");
		return;
	}

	snprintf(buf, 1024, "fill_screen: 0 0 0\n"
	"line_h: 0 105 319 105 5 255 255 255\n"
	"line_h: 0 210 319 210 5 255 255 255\n"
	"line_h: 160 0 160 210 5 255 255 255\n"
	"string: str {%s} 5 215 255 255 255\n"
	"gradient: 130 10 150 90 %d 0 %d %d 0 %d\n"
	"string: str {%.02f C} 5 40 255 255 255\n"
	"string: str {%d mm} 170 40 255 255 255\n"
	"line_h: 295 10 295 70 10 97 0 145\n"
	"triangle: 285 70 305 70 295 90 97 0 145\n"
	"string: str {Altitude:} 5 115 255 255 255\n"
	"string: str {%d m} 5 155 255 255 255\n"
	"circle_sq: 305 190 10 0 0 255\n"
	"triangle: 295 186 315 186 305 170 0 0 255\n"
	"circle_sq: 275 190 10 0 0 255\n"
	"triangle: 265 186 285 186 275 170 0 0 255\n"
	"circle_sq: 290 160 10 0 0 255\n"
	"triangle: 280 156 300 156 290 140 0 0 255\n"
	"string: str {%.02f%} 165 140 255 255 255\n"
	"update_screen:\n", time, r0, b0, 0, r0, temp, press, alt, hum );
	fputs(buf, ili9341);
	fclose(ili9341);
}

long get_value(char *buf){
        char *end, *temp_s;
        long temp;
        temp_s = strtok(buf,":\n");
        temp_s = strtok(NULL,":\n");
        return strtol(temp_s, &end, 10);
}

int main (void)
{
        char *time_s, buf[80] = "";
        double temperature, humidity, pressure;
        int altitude;
        FILE *bme280;

        while(1){
                bme280 = fopen ("/dev/bme280", "r");
                if (bme280 == NULL) {
                        perror ("Error open");
                        return -1;
                }
		fgets(buf, 50, bme280);
		time_s = strtok(buf, ":");
		time_s = strtok(NULL, "\n");
		time_s = strdup(&time_s[1]);
		printf("%s\n", time_s);

		fgets(buf, 50, bme280);
		temperature = (double)(get_value(buf))/100;
		printf("Temperature: %.02f C\n", temperature);

		fgets(buf, 50, bme280);
		pressure = (double)(get_value(buf))/256;
		printf("Pressure: %.02f mm\n", pressure/133);
		altitude = (int)((1 - pow(pressure/101325.0, 1/5.255))*44330);
		printf("Altitude: %d m\n", altitude);

		fgets(buf, 50, bme280);
		humidity = (double)(get_value(buf))/1024;
		printf("Humidity: %.02f\%\n\n", humidity);

		fclose(bme280);
		update_screen(temperature, (int)pressure/133, altitude, humidity, time_s);
		free(time_s);
		sleep(1);
        }
        return 0;
}


