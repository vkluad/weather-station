# ili9341 char device dirver

## How to use
1) Compile driver in target machine, or use directory with linux-headers, and some parameters
for this PC.
- Run `make all` & `make tree` in target machine.
- Run `make KDIR=${BUILD_DIR} all` for cross-compile.
2) Run this module
- `sudo insmod ili9341.ko`
3) Module will create file in `/dev/` directory,
- file `ili9341` in `/dev` is char device file, you can be written and cannot be read.

### For output some pictures to display you have 2 ways

1) Create text file with command and write in `/dev/ili9341` file:
- `# cat command_file > /dev/ili9341`

2) Create program who write to `/dev/ili9341` some command;

> Examples for previous targets you can find in `examples` directory

### Commands and syntax:
#### Syntax:
- `target: * * * ...`

#### Commands:
```
rectangle: x y width height r g b

circle: x y radius r g b

circle_sq: x y radius r g b

triangle: x1 y1 x2 y2 x3 y3 r g b

line: x1 y1 x2 y2 r g b

line_h: x1 y1 x2 y2 height r g b

gradient: x1 y1 x2 y2 r1 g1 b1 r2 g2 b2

string: x y str {some string} r g b

fill_screen: r g b

update_screen:
```

## Warnings !!!
1) str in string must be without `\n` symbol
2) `\n` symbol must be only in the end of the line
3) Function will be ignored if be not enough parameters 
