#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

void write_fff(char *buf)
{
	FILE *fd;

	fd = fopen ("/dev/ili9341", "w");
	if (fd == NULL) {
		perror ("Error open");
		return;
	}

	fprintf(fd, "%s", buf);

	fclose(fd);
}

int main (void)
{
	write_fff("fill_screen: 0 255 0\nstring:60 60 Hello everyone 255 0 0\nupdate_screen:");
	sleep(1);
	write_fff( "fill_screen: 255 255 0\nstring:70 70 Hello everyone 255 100 0\nupdate_screen:");
	sleep(1);
	write_fff( "fill_screen: 0 255 0\nstring:60 60 Hello everyone 255 0 0\nupdate_screen:");

	return 0;
}
