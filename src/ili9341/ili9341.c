#include <linux/init.h>
#include <linux/module.h>
#include <linux/spi/spi.h>
#include <linux/delay.h>
#include <linux/gpio.h>
#include <linux/device.h>
#include "ili9341.h"
#include "fonts.c"

MODULE_LICENSE("GPL");
MODULE_VERSION("1.0");
MODULE_AUTHOR("Vlad Kudenchuk <vkluad@gmail.com>");
MODULE_DESCRIPTION("LCD /dev driver for final task");

struct ili9341_data {
	struct spi_device *lcd_spi_device;
	void (*init)(struct ili9341_data *);
	u16 frame_buffer[LCD_WIDTH * LCD_HEIGHT];
};

static int major;

static char *command_cdev[] = { "rectangle",	"circle", "circle_sq",
				"triangle",	"line",	  "line_h",
				"gradient",	"string", "fill_screen",
				"update_screen" };

static void lcd_reset(void)
{
	gpio_set_value(LCD_PIN_RESET, 0);
	mdelay(5);
	gpio_set_value(LCD_PIN_RESET, 1);
}

static void lcd_write_cmd_data(struct ili9341_data *lcd_data, u8 cmd, u8 *data,
			       size_t data_size)
{
	u8 *buf;

	gpio_set_value(LCD_PIN_DC, 0);
	spi_write(lcd_data->lcd_spi_device, &cmd, sizeof(cmd));

	if (!data)
		return;

	buf = kmemdup(data, data_size, GFP_KERNEL);
	gpio_set_value(LCD_PIN_DC, 1);
	spi_write(lcd_data->lcd_spi_device, buf, data_size);
	kfree(buf);
}

#define lcd_write_command(lcd_data, cmd, param...)                             \
	({                                                                     \
		uint8_t d[] = { param };                                       \
		if (sizeof(d) == 0)                                            \
			lcd_write_cmd_data(lcd_data, cmd, NULL, 0);            \
		else                                                           \
			lcd_write_cmd_data(lcd_data, cmd, d, ARRAY_SIZE(d));   \
	})

static void lcd_set_address_window(struct ili9341_data *lcd_data, u16 x0,
				   u16 y0, u16 x1, u16 y1)
{
	lcd_write_command(lcd_data, LCD_CASET, (x0 >> 8) & 0xFF, x0 & 0xFF,
			  (x1 >> 8) & 0xFF, x1 & 0xFF);

	lcd_write_command(lcd_data, LCD_RASET, (y0 >> 8) & 0xFF, y0 & 0xFF,
			  (y1 >> 8) & 0xFF, y1 & 0xFF);

	lcd_write_command(lcd_data, LCD_RAMWR);
}

inline void lcd_update_screen(struct ili9341_data *lcd_data)
{
	lcd_write_cmd_data(lcd_data, LCD_RAMWR, (u8 *)lcd_data->frame_buffer,
			   sizeof(u16) * LCD_WIDTH * LCD_HEIGHT);
}

void lcd_draw_pixel(struct ili9341_data *lcd_data, u16 x, u16 y, u16 color)
{
	if ((x >= LCD_WIDTH) || (y >= LCD_HEIGHT)) {
		return;
	}

	lcd_data->frame_buffer[x + LCD_WIDTH * y] = (color >> 8) | (color << 8);
	lcd_update_screen(lcd_data);
}

void lcd_draw_circle(struct ili9341_data *lcd_data, u16 x1, u16 y1, u16 r,
		     u16 color)
{
	int x = 0, y = r, delta = 1 - 2 * r, error = 0;

	if ((x >= LCD_WIDTH) || (y >= LCD_HEIGHT))
		return;

	while (y >= x) {
		lcd_data->frame_buffer[x1 + x + LCD_WIDTH * (y1 + y)] =
			(color >> 8) | (color << 8);
		lcd_data->frame_buffer[x1 + x + LCD_WIDTH * (y1 - y)] =
			(color >> 8) | (color << 8);
		lcd_data->frame_buffer[x1 - x + LCD_WIDTH * (y1 + y)] =
			(color >> 8) | (color << 8);
		lcd_data->frame_buffer[x1 - x + LCD_WIDTH * (y1 - y)] =
			(color >> 8) | (color << 8);
		lcd_data->frame_buffer[x1 + y + LCD_WIDTH * (y1 + x)] =
			(color >> 8) | (color << 8);
		lcd_data->frame_buffer[x1 + y + LCD_WIDTH * (y1 - x)] =
			(color >> 8) | (color << 8);
		lcd_data->frame_buffer[x1 - y + LCD_WIDTH * (y1 + x)] =
			(color >> 8) | (color << 8);
		lcd_data->frame_buffer[x1 - y + LCD_WIDTH * (y1 - x)] =
			(color >> 8) | (color << 8);
		error = 2 * (delta + y) + 1;
		if ((delta < 0) && (error <= 0)) {
			delta += 2 * ++x + 1;
			continue;
		}
		if ((delta > 0) && (error > 0)) {
			delta -= 2 * --y + 1;
			continue;
		}
		delta += 2 * (++x - --y);
	}
}

void lcd_draw_circle_squere(struct ili9341_data *lcd_data, u16 x, u16 y, u16 r,
			    u16 color)
{
	u16 i, j;
	lcd_draw_circle(lcd_data, x, y, r, color);
	if ((x >= LCD_WIDTH) || (y >= LCD_HEIGHT))
		return;
	/* (i+j)^2+(j+y)^2=r^2 */
	for (i = x - r; i <= x + r; i++) {
		for (j = y - r; j <= y + r; j++) {
			if (((i - x) * (i - x) + (j - y) * (j - y)) <= r * r)
				lcd_data->frame_buffer[i + LCD_WIDTH * j] =
					(color >> 8) | (color << 8);
		}
	}
}

void lcd_draw_line(struct ili9341_data *lcd_data, u16 x0, u16 y0, u16 x1,
		   u16 y1, u16 color)
{
	/* Brezenham algorithm for draw line */
	const int delta_x = x0 < x1 ? x1 - x0 : x0 - x1;
	const int delta_y = y0 < y1 ? y1 - y0 : y0 - y1;
	const int sign_x = x0 < x1 ? 1 : -1;
	const int sign_y = y0 < y1 ? 1 : -1;
	int error = delta_x - delta_y;
	lcd_data->frame_buffer[x1 + LCD_WIDTH * y1] =
		(color >> 8) | (color << 8);

	if ((x0 >= LCD_WIDTH) || (y0 >= LCD_HEIGHT))
		return;

	if ((x1 >= LCD_WIDTH) || (y1 >= LCD_HEIGHT))
		return;

	while ((x0 != x1) || (y0 != y1)) {
		int error2 = error * 2;
		lcd_data->frame_buffer[x0 + LCD_WIDTH * y0] =
			(color >> 8) | (color << 8);
		if (error2 > -delta_y) {
			error -= delta_y;
			x0 += sign_x;
		}
		if (error2 < delta_x) {
			error += delta_x;
			y0 += sign_y;
		}
	}
}

void lcd_draw_line_h(struct ili9341_data *lcd_data, u16 x0, u16 y0, u16 x1,
		     u16 y1, u16 h, u16 color)
{
	/* draw bold line */
	int k, nx0 = x0, nx1 = x1, ny0 = y0, ny1 = y1;
	const int delta_x = x0 < x1 ? x1 - x0 : x0 - x1;

	if ((x0 >= LCD_WIDTH) || (y0 >= LCD_HEIGHT))
		return;

	if ((x1 >= LCD_WIDTH) || (y1 >= LCD_HEIGHT))
		return;

	for (k = -h / 2; k <= h / 2; k++) {
		if (delta_x < h) {
			nx0 = x0 + k > 0 ? x0 + k : 0;
			nx1 = x1 + k > 0 ? x1 + k : 0;
		} else {
			ny0 = y0 + k > 0 ? y0 + k : 0;
			ny1 = y1 + k > 0 ? y1 + k : 0;
		}
		lcd_draw_line(lcd_data, nx0, ny0, nx1, ny1, color);
	}
}

void lcd_draw_triangle(struct ili9341_data *lcd_data, u16 x1, u16 y1, u16 x2,
		       u16 y2, u16 x3, u16 y3, u16 color)
{
	/* algorithm for triangle rasterization https://compgraphics.info/2D/triangle_rasterization.php */
	int dx13 = 0, dx12 = 0, dx23 = 0, wx1, wx2, _dx13, i, j;
	if ((x1 >= LCD_WIDTH) || (y1 >= LCD_HEIGHT))
		return;

	if ((x2 >= LCD_WIDTH) || (y2 >= LCD_HEIGHT))
		return;

	if ((x3 >= LCD_WIDTH) || (y3 >= LCD_HEIGHT))
		return;

	/* sort*/
	if (y2 < y1) {
		swap(y1, y2);
		swap(x1, x2);
	}
	if (y3 < y1) {
		swap(y1, y3);
		swap(x1, x3);
	}
	if (y2 > y3) {
		swap(y2, y3);
		swap(x2, x3);
	}
	/*      */

	if (y3 != y1) {
		dx13 = x3 - x1;
		dx13 = dx13 * 1000 / (y3 - y1);
	}

	if (y2 != y1) {
		dx12 = x2 - x1;
		dx12 = dx12 * 1000 / (y2 - y1);
	} else {
		dx12 = 0;
	}

	if (y3 != y2) {
		dx23 = x3 - x2;
		dx23 = dx23 * 1000 / (y3 - y2);
	} else {
		dx23 = 0;
	}
	wx1 = x1 * 1000;
	wx2 = wx1;
	_dx13 = dx13;

	if (dx13 > dx12)
		swap(dx13, dx12);

	for (i = y1; i < y2; i++) {
		for (j = wx1 / 1000; j <= wx2 / 1000; j++) {
			lcd_data->frame_buffer[j + LCD_WIDTH * i] =
				(color >> 8) | (color << 8);
		}
		wx1 += dx13;
		wx2 += dx12;
	}

	if (y1 == y2) {
		wx1 = x1 * 1000;
		wx2 = x2 * 1000;
	}

	if (_dx13 < dx23)
		swap(_dx13, dx23);

	for (i = y2; i <= y3; i++) {
		for (j = wx1 / 1000; j <= wx2 / 1000; j++) {
			lcd_data->frame_buffer[j + LCD_WIDTH * i] =
				(color >> 8) | (color << 8);
		}
		wx1 += _dx13;
		wx2 += dx23;
	}
}

void lcd_fill_rectangle(struct ili9341_data *lcd_data, u16 x, u16 y, u16 w,
			u16 h, u16 color)
{
	u16 i;
	u16 j;

	if ((x >= LCD_WIDTH) || (y >= LCD_HEIGHT)) {
		return;
	}

	if ((x + w - 1) > LCD_WIDTH) {
		w = LCD_WIDTH - x;
	}

	if ((y + h - 1) > LCD_HEIGHT) {
		h = LCD_HEIGHT - y;
	}

	for (j = 0; j < h; j++) {
		for (i = 0; i < w; i++) {
			lcd_data->frame_buffer[(x + LCD_WIDTH * y) +
					       (i + LCD_WIDTH * j)] =
				(color >> 8) | (color << 8);
		}
	}
}

void lcd_fill_gradient(struct ili9341_data *lcd_data, u16 x0, u16 y0, u16 x1,
		       u16 y1, u16 r0, u16 g0, u16 b0, u16 r1, u16 g1, u16 b1)
{
	int i, j, delta_r, delta_g, delta_b;
	delta_r = (r1 > r0) ? 1 : -1;
	delta_g = (g1 > g0) ? 1 : -1;
	delta_b = (b1 > b0) ? 1 : -1;

	if ((x0 >= LCD_WIDTH) || (y0 >= LCD_HEIGHT))
		return;

	if ((x1 >= LCD_WIDTH) || (y1 >= LCD_HEIGHT))
		return;

	if ((r0 > 255) || (g0 > 255) || (b0 > 255))
		return;

	if ((r1 > 255) || (g1 > 255) || (b1 > 255))
		return;

	for (i = y0; i <= y1; i++) {
		if (r0 != r1)
			r0 += delta_r;
		if (g0 != g1)
			g0 += delta_g;
		if (b0 != b1)
			b0 += delta_b;
		for (j = x0; j <= x1; j++)
			lcd_data->frame_buffer[j + LCD_WIDTH * i] =
				(COLOR_COLOR565(r0, g0, b0) >> 8) |
				(COLOR_COLOR565(r0, g0, b0) << 8);
	}
}

void lcd_fill_screen(struct ili9341_data *lcd_data, u16 color)
{
	lcd_fill_rectangle(lcd_data, 0, 0, LCD_WIDTH - 1, LCD_HEIGHT - 1,
			   color);
}

static void lcd_put_char(struct ili9341_data *lcd_data, u16 x, u16 y, char ch,
			 FontDef font, u16 color)
{
	u32 i, b, j;
	if ((x >= LCD_WIDTH) || (y >= LCD_HEIGHT))
		return;

	for (i = 0; i < font.height; i++) {
		b = font.data[(ch - 32) * font.height + i];
		for (j = 0; j < font.width; j++) {
			if ((b << j) & 0x8000) {
				lcd_data->frame_buffer[(x + LCD_WIDTH * y) +
						       (j + LCD_WIDTH * i)] =
					(color >> 8) | (color << 8);
			}

		}
	}
}

void lcd_put_str(struct ili9341_data *lcd_data, u16 x, u16 y, const char *str,
		 FontDef font, u16 color)
{
	if ((x >= LCD_WIDTH) || (y >= LCD_HEIGHT))
		return;

	while (*str) {
		if (x + font.width >= LCD_WIDTH) {
			x = 0;
			y += font.height;
			if (y + font.height >= LCD_HEIGHT) {
				break;
			}

			if (*str == ' ') {
				// skip spaces in the beginning of the new line
				str++;
				continue;
			}
		}
		lcd_put_char(lcd_data, x, y, *str, font, color);
		x += font.width;
		str++;
	}
}

void lcd_init_ili9341(struct ili9341_data *lcd_data)
{
	// SOFTWARE RESET
	lcd_write_command(lcd_data, 0x01);
	mdelay(1000);

	// POWER CONTROL A
	lcd_write_command(lcd_data, 0xCB, 0x39, 0x2C, 0x00, 0x34, 0x02);

	// POWER CONTROL B
	lcd_write_command(lcd_data, 0xCF, 0x00, 0xC1, 0x30);

	// DRIVER TIMING CONTROL A
	lcd_write_command(lcd_data, 0xE8, 0x85, 0x00, 0x78);

	// DRIVER TIMING CONTROL B
	lcd_write_command(lcd_data, 0xEA, 0x00, 0x00);

	// POWER ON SEQUENCE CONTROL
	lcd_write_command(lcd_data, 0xED, 0x64, 0x03, 0x12, 0x81);

	// PUMP RATIO CONTROL
	lcd_write_command(lcd_data, 0xF7, 0x20);

	// POWER CONTROL,VRH[5:0]
	lcd_write_command(lcd_data, 0xC0, 0x23);

	// POWER CONTROL,SAP[2:0];BT[3:0]
	lcd_write_command(lcd_data, 0xC1, 0x10);

	// VCM CONTROL
	lcd_write_command(lcd_data, 0xC5, 0x3E, 0x28);

	// VCM CONTROL 2
	lcd_write_command(lcd_data, 0xC7, 0x86);

	// PIXEL FORMAT
	lcd_write_command(lcd_data, 0x3A, 0x55);

	// FRAME RATIO CONTROL, STANDARD RGB COLOR
	lcd_write_command(lcd_data, 0xB1, 0x00, 0x18);

	// DISPLAY FUNCTION CONTROL
	lcd_write_command(lcd_data, 0xB6, 0x08, 0x82, 0x27);

	// 3GAMMA FUNCTION DISABLE
	lcd_write_command(lcd_data, 0xF2, 0x00);

	// GAMMA CURVE SELECTED
	lcd_write_command(lcd_data, 0x26, 0x01);

	// POSITIVE GAMMA CORRECTION
	lcd_write_command(lcd_data, 0xE0, 0x0F, 0x31, 0x2B, 0x0C, 0x0E, 0x08,
			  0x4E, 0xF1, 0x37, 0x07, 0x10, 0x03, 0x0E, 0x09, 0x00);

	// NEGATIVE GAMMA CORRECTION
	lcd_write_command(lcd_data, 0xE1, 0x00, 0x0E, 0x14, 0x03, 0x11, 0x07,
			  0x31, 0xC1, 0x48, 0x08, 0x0F, 0x0C, 0x31, 0x36, 0x0F);

	// EXIT SLEEP
	lcd_write_command(lcd_data, 0x11);
	mdelay(120);

	// TURN ON DISPLAY
	lcd_write_command(lcd_data, 0x29);

	// MEMORY ACCESS CONTROL
	lcd_write_command(lcd_data, 0x36, 0x28);

	// INVERSION
	//	lcd_write_command(0x21);
}

static struct ili9341_data lcd_data;

static int ili9341_probe(struct spi_device *spi)
{
	lcd_data.lcd_spi_device = spi;
	lcd_data.init = lcd_init_ili9341;

	gpio_request(LCD_PIN_RESET, "LCD_PIN_RESET");
	gpio_direction_output(LCD_PIN_RESET, 0);
	gpio_request(LCD_PIN_DC, "LCD_PIN_DC");
	gpio_direction_output(LCD_PIN_DC, 0);
	lcd_reset();

	lcd_data.init(&lcd_data);

	memset(lcd_data.frame_buffer, COLOR_BLACK,
	       sizeof(lcd_data.frame_buffer));
	lcd_set_address_window(&lcd_data, 0, 0, LCD_WIDTH - 1, LCD_HEIGHT - 1);

	lcd_update_screen(&lcd_data);

	pr_info("LCD: module loaded\n");
	return 0;
}

static int ili9341_remove(struct spi_device *spi)
{
	gpio_free(LCD_PIN_DC);
	gpio_free(LCD_PIN_RESET);
	return 0;
}

static const struct of_device_id ili9341_of_match[] = {
	{ .compatible = "vkluad,ili9341" },
	{}
};
MODULE_DEVICE_TABLE(of, ili9341_of_match);

static const struct spi_device_id ili9341_id[] = { { "ili9341", 0 }, {} };
MODULE_DEVICE_TABLE(spi, ili9341_id);

static struct spi_driver ili9341_spi_driver = {
	.driver = {
		.name = "ili9341",
		.of_match_table = ili9341_of_match,
	},
	.id_table = ili9341_id,
	.probe = ili9341_probe,
	.remove = ili9341_remove,
};

#define point_to_return(n)                                                     \
	({                                                                     \
		int j;                                                         \
		for (j = 0; j <= n; j++)                                       \
			if (num_arr[j] < 0) {                                  \
				printk(DRIVER_NAME                             \
			       ": not enough parameters, must be %d", n);      \
				return;                                        \
			}                                                      \
	})

void command_separator(struct ili9341_data *lcd_data, char *buf)
{
	char *tok, *i_tok, str[128] = "";
	long num_arr[16];
	int i = 0, j = 0;
	while ((tok = strsep(&buf, ":")) != NULL) {
		for (i = 0; i < sizeof(command_cdev) / sizeof(*command_cdev); i++) {
			if (tok == NULL)
				break;
			if ((strncmp(tok, command_cdev[i],
				    strlen(tok)) == 0) &&
				    (strncmp(tok, command_cdev[i],
				    strlen(command_cdev[i])) == 0)) {
				strncpy(str, "\000", strlen(str));
				memset(num_arr, -1, sizeof(num_arr));
				tok = strsep(&buf, "\n");
				while ((i_tok = strsep(&tok, " ")) != NULL) {
					if (kstrtol(i_tok, 10, &num_arr[j]) == 0)
						j++;

					if( strncmp(i_tok, "str", 3) == 0){
						i_tok = strsep(&tok, "{");
						i_tok = strsep(&tok, "}");
						strncat(str, i_tok, strlen(i_tok));
					}
				}

				j = 0;
				switch (i) {
				case 0:
					point_to_return(6);
					lcd_fill_rectangle(
						lcd_data, num_arr[0],
						num_arr[1], num_arr[2],
						num_arr[3],
						COLOR_COLOR565(num_arr[4],
							       num_arr[5],
							       num_arr[6]));
					break;
				case 1:
					point_to_return(5);
					lcd_draw_circle(
						lcd_data, num_arr[0],
						num_arr[1], num_arr[2],
						COLOR_COLOR565(num_arr[3],
							       num_arr[4],
							       num_arr[5]));
					break;
				case 2:
					point_to_return(5);
					lcd_draw_circle_squere(
						lcd_data, num_arr[0],
						num_arr[1], num_arr[2],
						COLOR_COLOR565(num_arr[3],
							       num_arr[4],
							       num_arr[5]));
					break;
				case 3:
					point_to_return(8);
					lcd_draw_triangle(
						lcd_data, num_arr[0],
						num_arr[1], num_arr[2],
						num_arr[3], num_arr[4],
						num_arr[5],
						COLOR_COLOR565(num_arr[6],
							       num_arr[7],
							       num_arr[8]));
					break;
				case 4:
					point_to_return(6);
					lcd_draw_line(
						lcd_data, num_arr[0],
						num_arr[1], num_arr[2],
						num_arr[3],
						COLOR_COLOR565(num_arr[4],
							       num_arr[5],
							       num_arr[6]));
					break;
				case 5:
					point_to_return(7);
					lcd_draw_line_h(
						lcd_data, num_arr[0],
						num_arr[1], num_arr[2],
						num_arr[3], num_arr[4],
						COLOR_COLOR565(num_arr[5],
							       num_arr[6],
							       num_arr[7]));
					break;
				case 6:
					point_to_return(9);
					lcd_fill_gradient(
						lcd_data, num_arr[0],
						num_arr[1], num_arr[2],
						num_arr[3], num_arr[4],
						num_arr[5], num_arr[6],
						num_arr[7], num_arr[8],
						num_arr[9]);
					break;
				case 7:
					point_to_return(4);
					lcd_put_str(lcd_data, num_arr[0],
						    num_arr[1], str, Font_16x26,
						    COLOR_COLOR565(num_arr[2],
								   num_arr[3],
								   num_arr[4]));
					break;
				case 8:
					point_to_return(2);
					lcd_fill_screen(
						lcd_data,
						COLOR_COLOR565(num_arr[0],
							       num_arr[1],
							       num_arr[2]));
					break;
				case 9:
					lcd_update_screen(lcd_data);
					break;
				default:
					printk(DRIVER_NAME
					       ": Error from command separator\n");
					break;
				}
			}
		}
	}
}

ssize_t ili9341_write(struct file *file, const char __user *buf, size_t lenght,
		      loff_t *pos)
{
	char *temp_buf = kcalloc(lenght, sizeof(char), GFP_KERNEL);
	if (copy_from_user(temp_buf, buf, lenght)) {
		lenght = -EINVAL;
		goto end;
	}
	command_separator(&lcd_data, temp_buf);

end:
	kfree(temp_buf);
	return lenght;
}

static struct class *ili9341_class;
static struct cdev ili9341_cdev;

static const struct file_operations ili9341_cdev_fops = {
	.owner = THIS_MODULE,
	.write = ili9341_write,
};

static void __exit mod_exit(void)
{
	dev_t dev;
	dev = MKDEV(major, 0);

	device_destroy(ili9341_class, dev);
	class_destroy(ili9341_class);
	cdev_del(&ili9341_cdev);

	unregister_chrdev_region(MKDEV(major, 0), 1);

	spi_unregister_driver(&ili9341_spi_driver);
}

static int __init mod_init(void)
{
	int ret;
	dev_t dev;

	ret = spi_register_driver(&ili9341_spi_driver);
	if (ret) {
		printk(DRIVER_NAME ": failed to add new spi driver");
		return ret;
	}

	ret = alloc_chrdev_region(&dev, 0, 1, DRIVER_NAME);

	if (ret < 0) {
		printk(KERN_ERR DRIVER_NAME
		       ": Device could not be allocated!\n");
		return ret;
	}
	major = MAJOR(dev);

	cdev_init(&ili9341_cdev, &ili9341_cdev_fops);
	ili9341_cdev.owner = THIS_MODULE;
	ret = cdev_add(&ili9341_cdev, dev, 1);

	if (ret < 0) {
		printk(KERN_ERR DRIVER_NAME ": Can not add char device\n");
		goto dev_err;
	}

	ili9341_class = class_create(THIS_MODULE, DRIVER_NAME);

	if (IS_ERR(ili9341_class)) {
		printk(KERN_ERR DRIVER_NAME ": bad class sysfs create\n");
		goto cdev_err;
	}

	device_create(ili9341_class, NULL, dev, NULL,
		      DRIVER_NAME); // Create cdev

	return 0;

cdev_err:
	cdev_del(&ili9341_cdev);

dev_err:
	unregister_chrdev_region(MKDEV(major, 0), 1);

	return ret;
}

module_init(mod_init);
module_exit(mod_exit);
