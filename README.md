# Weather station
## Weather station based on raspberry pi zero w

### Used components:
- bme280
- 3.2inch RPI LCD V4
- raspberry pi zero w

### How to use
- `make all` & `make test` in root directory

# Results
### 1) LCD screen:
![Image01](res/display.jpg)

### 2) All components:
![Image02](res/all_metal.jpg)
