ifeq ($(KDIR),)
	KDIR = /lib/modules/$(shell uname -r)/build
endif

.PHONY: all bme280 ili9341 weather-station test clean

all: bme280 ili9341 weather-station
	@mkdir -p bin
	@cp src/bme280/bme280.ko ./bin/
	@cp src/ili9341/ili9341.ko ./bin/
	@cp src/weather-station/weather_station ./bin/

bme280:
	@cd src/bme280/ && make KDIR=$(KDIR) clean && make KDIR=$(KDIR) all

ili9341:
	@cd src/ili9341/ && make KDIR=$(KDIR) clean && make KDIR=$(KDIR) all

weather-station:
	@cd src/weather-station/ && make clean && make all

tree:
	@cd src/bme280/ && make tree
	@cd src/ili9341/ && make tree

clean:
	@cd src/bme280/ && make KDIR=$(KDIR) clean
	@cd src/ili9341/ && make KDIR=$(KDIR) clean
	@cd src/weather-station/ && make clean
	@rm -rf bin

test:
	@sudo insmod bin/ili9341.ko
	@sudo insmod bin/bme280.ko
	@sudo ./bin/weather_station

